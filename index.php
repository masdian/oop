<!DOCTYPE html>
<html>
<body>

<?php
// Release 0

require ('animal.php');
require ('Ape.php');
require ('Frog.php');

$sheep = new Animal("shaun");
echo $sheep -> name;
echo "<br>";
echo $sheep -> legs;
echo "<br>";
echo $sheep -> cold_blooded;
echo "<br><br>";

// Release 1
$sungokong = new Ape("kera sakti");
echo $sungokong -> name;
echo "<br>";
$sungokong -> yell();
echo "<br>";
echo $sungokong -> legs;
echo "<br>";
echo $sungokong -> cold_blooded;
echo "<br><br>";


$kodok = new Frog("buduk");
echo $kodok -> name;
echo "<br>";
$kodok -> jump() ; // "hop hop"
echo "<br>";
echo $kodok -> legs;
echo "<br>";
echo $kodok -> cold_blooded;
echo "<br>";

?>
 
</body>
</html>